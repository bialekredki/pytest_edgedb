from unittest.mock import DEFAULT, MagicMock, patch

import pytest
from edgedb.blocking_client import Client
from edgedb.errors import ClientConnectionError

from tests.unitests.utils import MockedPopen

TEST_EXAMPLE = """
        import pytest
        @pytest.mark.edgedb
        def test_example():
            assert True
        """


def test_edgedb_database_creation__skipped_if_exists(
    pytester: pytest.Pytester,
):
    with patch.object(
        Client, "ensure_connected", MagicMock(return_value=None)
    ) as mocked_connection, patch(
        "subprocess.Popen", MockedPopen(output=(b"OK:", b""))
    ) as mocked_process:
        pytester.makepyfile(TEST_EXAMPLE)
        result = pytester.runpytest()
        assert mocked_connection.call_count == 2
        assert mocked_process.call_count == 2


@patch(
    "edgedb.blocking_client.Client.ensure_connected",
    MagicMock(side_effect=ClientConnectionError("Test")),
)
def test_edgedb_database_creation__marked_tests_are_skipped_if_no_connection(
    pytester: pytest.Pytester,
):
    pytester.makepyfile(TEST_EXAMPLE)
    result = pytester.runpytest()
    assert result.ret == pytest.ExitCode.NO_TESTS_COLLECTED


def test_edgedb_database_creation__succeeds(
    pytester: pytest.Pytester,
):
    with patch.multiple(
        "edgedb.blocking_client.Client", ensure_connected=DEFAULT, close=DEFAULT
    ) as mocks, patch(
        "subprocess.Popen",
        MockedPopen(
            output=(
                (b"OK: CREATE DATABAES", b""),
                (b"OK", b""),
                (b"OK: DROP DATABASE", b""),
            )
        ),
    ) as mocked_popen:
        mocks["ensure_connected"].return_value = None
        mocks["close"].return_value = None
        pytester.makepyfile(TEST_EXAMPLE)

        res = pytester.runpytest()

    assert res.ret == pytest.ExitCode.OK
    res.assert_outcomes(passed=1)
    # We called subprocesses three times: Creation, migration, teardown
    assert mocked_popen.call_count == 3
    assert mocks["close"].call_count == 2
    assert mocks["ensure_connected"].call_count == 2
