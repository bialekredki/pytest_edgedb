from pytest_edgedb.types import PytestCLIStr


def test_pytest_cli_string():
    assert PytestCLIStr("--test").dest == "test"
