from unittest.mock import MagicMock, patch

import pytest

from pytest_edgedb.cli import Command, CommandResult, retryable_command

SimpleTestCommand = Command("test")


@pytest.fixture(name="subprocess_success_result", scope="session")
def fixture_subprocess_success_result():
    yield (b"OK: Success", b"")


@pytest.fixture(name="mocked_success_communicate")
def fixture_mocked_success_communicate(subprocess_success_result: tuple[bytes, bytes]):
    with patch(
        "subprocess.Popen.communicate",
        MagicMock(return_value=subprocess_success_result),
    ) as mock:
        yield mock


@patch("subprocess.Popen.communicate", MagicMock(return_value=(b"", b"")))
def test_simple_command__empty_pipes():
    result = SimpleTestCommand()
    assert result.is_ok
    assert not result.message


@patch("subprocess.Popen.communicate", MagicMock(return_value=(b"Test", b"")))
def test_simple_command__empty_stderr():
    result = SimpleTestCommand()
    assert result.is_ok
    assert result.message == "Test"


@patch("subprocess.Popen.communicate", MagicMock(return_value=(b"", b"Test")))
def test_simple_command__empty_stdout():
    result = SimpleTestCommand()
    assert result.is_ok
    assert result.message == "Test"


@patch("subprocess.Popen.communicate", MagicMock(return_value=(b"Test", b"Test")))
def test_simple_command__nonempty_both_pipes():
    result = SimpleTestCommand()
    assert result.is_ok
    assert result.message == "TestTest"


@pytest.mark.parametrize("check", ("OK:", b"OK", r"^OK:"))
@pytest.mark.usefixtures("mocked_success_communicate")
def test_command_with_result_check__success(check: str | bytes):
    command = Command("test", result_check=check)
    result = command()
    assert result.is_ok


@pytest.mark.usefixtures("mocked_success_communicate")
def test_command_with_result_check__fail():
    command = Command("test", result_check="Something")
    result = command()
    assert not result.is_ok


def test_command_formatting__simple_command():
    assert SimpleTestCommand.formatted_command() == ["test"]


def test_command_formatting__formattable_command():
    command = Command("test {keyword}")
    assert command.formatted_command(keyword="z") == ["test", "z"]


def test_command_formatting__raise():
    command = Command("test {keyword}")
    with pytest.raises(KeyError):
        command.formatted_command()


def test_command_formatting__with_simple_query():
    command = Command("test", query="simple")
    assert command.formatted_command() == ["test", "simple"]


def test_command_formatting__with_space_separated_query():
    command = Command("test", query="simple simple")
    assert command.formatted_command() == ["test", "simple simple"]


def test_command_formatting__with_formatabble_query():
    command = Command("test", query="create database {name}")
    assert command.formatted_command(name="test") == ["test", "create database test"]


def test_command_formatting__with_both_formatabble_command():
    command = Command("test {ckwarg} apply", query="create database {qkwarg}")
    assert command.formatted_command(ckwarg="migration", qkwarg="name") == [
        "test",
        "migration",
        "apply",
        "create database name",
    ]


@pytest.mark.parametrize("retries_limit", (1, 10, 100, 1000))
@patch("time.sleep", MagicMock(return_value=None))
def test_retryable_command__allow_for_maximal_retries(retries_limit: int):
    with patch.object(
        Command,
        "__call__",
        side_effect=[CommandResult(False, "") for _ in range(retries_limit - 1)]
        + [CommandResult(True, "Finally got the job done!")],
    ):
        command = retryable_command(retries_limit=retries_limit)(Command("test"))
        result = command()

    assert result.is_ok
    assert result.message == "Finally got the job done!"


@patch.object(Command, "__call__", return_value=CommandResult(False, ""))
def test_retyrable_command__fail_after_maximal_retries(_):
    command = retryable_command(timeout=10, retries_limit=1_000, wait_time=0)(
        SimpleTestCommand
    )
    with pytest.raises(AssertionError):
        command()
