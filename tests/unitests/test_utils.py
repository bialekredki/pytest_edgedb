import re

import pytest

from pytest_edgedb.utils import generate_safe_name


@pytest.mark.parametrize("_", range(100))
@pytest.mark.timeout(1)
def test_generate_safe_name(_):
    assert re.match(r"^[0-9]", generate_safe_name()) is None
