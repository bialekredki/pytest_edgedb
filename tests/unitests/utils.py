from collections.abc import Iterable
from typing import Any, TypeAlias

MockOutputType: TypeAlias = Iterable[tuple[bytes, bytes] | None] | tuple[bytes, bytes]


class MockedPopen:
    def __init__(
        self,
        args: Iterable[str] | None = None,
        *,
        output: MockOutputType = (b"", b""),
        echo: bool = False
    ) -> None:
        self.args = args
        self.output = output
        self.echo = echo
        self.call_count = 0

    def __enter__(self):
        return self

    def __exit__(self, _exc_type, _value, _traceback):
        pass

    def __call__(self, args: Iterable[str], **kwds: Any) -> "MockedPopen":
        if self.args is None:
            self.args = args
        self.call_count += 1
        return self

    def communicate(self, input=None, timeout=None):
        # TODO: Add error handling here
        if self.echo:
            return self.args
        if isinstance(self.output, Iterable):
            return self.output[self.call_count - 1]
        return self.output
