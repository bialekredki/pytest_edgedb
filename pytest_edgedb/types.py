from dataclasses import dataclass
from functools import cached_property
from typing import Any


class PytestCLIStr(str):
    @cached_property
    def dest(self):
        return self.replace("--", "")


@dataclass
class PytestCLIOption:
    name: PytestCLIStr
    action: str = "store"
    help: str | None = None
    type: Any = None
    default: Any = None
    metavar: Any = None

    def as_kwargs(self) -> dict:
        return {
            "help": self.help,
            "type": self.type,
            "dest": self.name.dest,
            "default": self.default,
            "metavar": self.metavar,
        }
