import logging
import re
import subprocess
import time
from collections import namedtuple
from datetime import datetime, timedelta
from functools import wraps
from re import Pattern
from typing import Any, Generic, TypeVar

logger = logging.getLogger("pytest-edgedb")
CommandResult = namedtuple("CommandResult", ("is_ok", "message"))

S = TypeVar("S", str, bytes, Pattern, None)


class Command(Generic[S]):
    # pylint: disable=arguments-differ
    def __init__(
        self, command: str, query: str | None = None, result_check: S = None
    ) -> None:
        self.__command = command
        self.__query = query
        self.__result_check = (
            result_check
            if not isinstance(result_check, bytes)
            else result_check.decode("utf-8")
        )

    def __call__(self, **kwds: Any) -> CommandResult:
        message = ""
        with subprocess.Popen(
            self.formatted_command(**kwds),
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        ) as proc:
            stdout, stderr = proc.communicate()
        if stdout:
            message += stdout.decode("utf-8")
        if stderr:
            message += stderr.decode("utf-8")

        return CommandResult(self.__is_message_correct(message=message), message)

    def __is_message_correct(self, message: str):
        if self.__result_check is None:
            return True
        return re.search(self.__result_check, message) is not None

    def formatted_command(self, **kwargs):
        command = self.__command.format(**kwargs).split(" ")
        if self.__query:
            command.append(self.__query.format(**kwargs))
        return command


def retryable_command(
    timeout: int = 5_000,
    retries_limit: int = 100,
    wait_time: float = 0.15,
    raise_on_timeout: bool = True,
):
    def _decorator(func: Command):
        @wraps(func)
        def _wrapper(*args, **kwargs):
            start = datetime.now()
            attempts = 0
            while (
                timedelta(milliseconds=timeout) >= datetime.now() - start
                and attempts < retries_limit
            ):
                attempts += 1
                res: CommandResult | Any = func(*args, **kwargs)
                if not isinstance(res, CommandResult):
                    logger.warning(
                        "Result from command %s is not of type %s",
                        func.__name__,
                        type(CommandResult),
                    )
                    if res is not None:
                        res = CommandResult(True, str(res))
                        break
                    raise RuntimeError("Command return None.")
                if res.is_ok:
                    break
                time.sleep(wait_time)
            if raise_on_timeout:
                assert (
                    res.is_ok
                ), f"Command {func.formatted_command(**kwargs)} failed. Error message {res.message}."
            return res

        return _wrapper

    return _decorator


Migrate = Command("edgedb migration apply")

CreateDatabase = retryable_command()(
    Command(
        "edgedb query",
        query="create database {name}",
        result_check=b"OK: CREATE DATABASE",
    )
)

DropDatabase = retryable_command()(
    Command(
        "edgedb query", query="drop database {name}", result_check=b"OK: DROP DATABASE"
    )
)
