import pytest
from edgedb import create_async_client, create_client


@pytest.fixture(name="edgedb_client", scope="session")
def fixture_client():
    client = create_client()
    yield client
    client.close(timeout=15)


@pytest.fixture(name="edgedb_async_client", scope="session")
async def fixture_async_client():
    client = create_async_client()
    yield client
    await client.aclose()
