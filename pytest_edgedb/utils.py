import re
from collections.abc import Callable
from functools import wraps

from edgedb import AsyncIOClient
from nanoid import generate

from pytest_edgedb.constants import ALPHANUMERIC_ALPHABET


def transactional(func: Callable, /, *, client: AsyncIOClient):
    @wraps(func)
    async def _wrapper(*args, **kwargs):
        # pylint: disable=protected-access,invalid-name
        async for tx in client.transaction():
            async with tx:
                await tx._ensure_transaction()
                await tx._privileged_execute("declare savepoint f1;")
                await func(tx, *args, **kwargs)
                await tx._privileged_execute("rollback to savepoint f1;")

    return _wrapper


def generate_safe_name():
    while True:
        name = generate(alphabet=ALPHANUMERIC_ALPHABET, size=6)
        if re.match(r"^[A-Za-z]", name):
            return name
