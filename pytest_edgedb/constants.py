from collections import namedtuple

from pytest_edgedb.types import PytestCLIOption, PytestCLIStr

_PytestCLIOptionsType = namedtuple(
    "_PytestCLIOptionsType",
    ("DATABASE_NAME", "DROP_DATABASE_AFTER_FINISH", "PERFORM_MIGRATION"),
)

PYTEST_CLI_OPTIONS = _PytestCLIOptionsType(
    PytestCLIOption(
        PytestCLIStr("--database"),
        help="If specified this database will be used in tests. If such database doesn't exist it will be created and migrations will be performed.",
        type=str,
    ),
    PytestCLIOption(
        PytestCLIStr("--drop_database"),
        help="Drop database at the end of session.",
        type=bool,
        default=True,
        metavar="True",
    ),
    PytestCLIOption(
        PytestCLIStr("--migrate"),
        help="Perform migration before session starts.",
        type=bool,
        default=True,
        metavar="True",
    ),
)


ALPHANUMERIC_ALPHABET = "1234567890QWERTYUOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm"


TEMPORARY_DATABASE_ENVVAR_NAME = "_TEMP_PYTEST_EDGEDB_DATABASE_NAME"
