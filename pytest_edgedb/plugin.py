# pylint: disable=wildcard-import,unused-wildcard-import
import os

import pytest
from edgedb import create_client
from edgedb.errors import ClientConnectionError, UnknownDatabaseError

from pytest_edgedb.cli import CreateDatabase, DropDatabase, Migrate
from pytest_edgedb.constants import PYTEST_CLI_OPTIONS, TEMPORARY_DATABASE_ENVVAR_NAME
from pytest_edgedb.fixtures import *
from pytest_edgedb.utils import generate_safe_name


def pytest_addoption(parser: pytest.Parser):
    group = parser.getgroup("edgedb")
    for option in PYTEST_CLI_OPTIONS:
        group.addoption(option.name, **option.as_kwargs())


def pytest_configure(config: pytest.Config):
    config.addinivalue_line(
        "markers",
        "edgedb(use_transaction=True): mark the test to run in edgedb transaction. Will pass transaction to the test if use_transaction is set to True.",
    )


def pytest_sessionstart(session: pytest.Session):
    os.environ[TEMPORARY_DATABASE_ENVVAR_NAME] = (
        session.config.getoption(PYTEST_CLI_OPTIONS.DATABASE_NAME.name.dest)
        or generate_safe_name()
    )


def pytest_sessionfinish():
    database_name = os.environ[TEMPORARY_DATABASE_ENVVAR_NAME]
    try:
        client = create_client(database=database_name)
        client.ensure_connected()
        DropDatabase(name=database_name)
    except Exception:
        pass  # TODO: Log warning here at least
    finally:
        client.close()
    del os.environ[TEMPORARY_DATABASE_ENVVAR_NAME]


def pytest_collection_modifyitems(items: list[pytest.Item]):
    database_name = os.environ[TEMPORARY_DATABASE_ENVVAR_NAME]
    try:
        client = create_client(database=database_name)
        client.ensure_connected()
        is_connected = True
    except ClientConnectionError:
        is_connected = False
    except UnknownDatabaseError:
        CreateDatabase(name=database_name)
        try:
            client.ensure_connected()
            is_connected = True
        except (ClientConnectionError, UnknownDatabaseError):
            is_connected = False
    finally:
        client.close()
    if is_connected:
        Migrate()
    ignore_items = []
    for item in items:
        marker = item.get_closest_marker("edgedb")
        if marker is None or marker.name != "edgedb":
            continue
        if not is_connected:
            ignore_items.append(item)
    for item in ignore_items:
        items.remove(item)
