## 0.0.1 (2023-02-19)

### Feat

- Add simple fixtures
- Add simplest implementation

### Fix

- Session start argument for getoption
